byType = True
nightOnTheGin = True
sortByTune = True

with open("all-current.abc") as abcFile:
        setName = ""
        tuneTypes = {}
        tuneSetPairs = []
        currentType = ""
        currentSetPairs = []
        while (True):
                line = abcFile.readline()
                if (line == ""):
                        break
                if (line[:2] == "X:"):
                        if (currentType != ""):
                                tuneTypes[currentType] = tuneTypes[currentType] + currentSetPairs
                        currentType = ""
                        currentSetPairs = []
                        setName = ""
                elif (line[:2] == "T:"):
                        if (setName == ""):
                                setName = line[2:-1]
                        else:
                                tuneSetPairs.append((line[2:-1], setName))
                                currentSetPairs.append((line[2:-1], setName))
                elif (line[:2] == "R:"):
                        currentType = line[2:-1].title()
                        if (not currentType in tuneTypes):
                                tuneTypes[currentType] = []

def stripAThe(string):
        if (string[:2] == "A " and nightOnTheGin):
                return string[2:]
        elif (string[:4] == "The "):
                return string[4:]
        else:
                return string

for tuneType in tuneTypes:
        tuneTypes[tuneType] = sorted(tuneTypes[tuneType], key=lambda x: stripAThe(x[0 if sortByTune else 1]))

sortedPairs = sorted(tuneSetPairs, key=lambda x: stripAThe(x[0 if sortByTune else 1]))

with open("index-current.csv","w+") as indexFile:
        if (byType):
                first = True
                for tuneType in tuneTypes:
                        thisTypeSortedPairs = tuneTypes[tuneType]
                        if (not first):
                                indexFile.write("\n")
                        first = False

                        indexFile.write(tuneType+"\n")
                        for pair in thisTypeSortedPairs:
                                indexFile.write(pair[0]+","+pair[1]+"\n")

        else:
                for pair in sortedPairs:
                        indexFile.write(pair[0]+","+pair[1]+"\n")
